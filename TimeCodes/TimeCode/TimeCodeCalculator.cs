﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace TimeCode
{
    public class Utility
    {
        //Validation
        public bool ValidateTimeCodeFormat(string timecode)
        {
            var regex = @"^[0-9]{2}:[0-9]{2}:[0-9]{2}:[0-9]{2}$";
            var match = Regex.Match(timecode, regex);
            if (!match.Success)
            {
                return false;
            }
            return true;
        }
        public string FormatTimeCodeToString(string timecode)
        {
            string[] timeCodeArray;
            if (ValidateTimeCodeFormat(timecode))
            {
                timeCodeArray = timecode.Split(':');
                StringBuilder sb = new StringBuilder();
                foreach (var item in timeCodeArray)
                {
                    sb.Append(item);
                }
                return sb.ToString();
            }
            return null;
        }
        public bool ValidateTimeUnit(int timeUnit)
        {
            var regex = @"^[0-9]";
            var match = Regex.Match(timeUnit.ToString(), regex);
            if (!match.Success)
            {
                return false;
            }
            return true;
        }

        public bool ValidateInputField(string inputField)
        {
            var inputTimeCode = FormatTimeCodeToString(inputField);
            if (String.IsNullOrWhiteSpace(inputTimeCode))
            {
                return false;
            }
            return true;
        }
        public bool ValidateOutputField(string timecode)
        {
            var regex = @"^[0-9]{8}$";
            var match = Regex.Match(timecode, regex);
            if (!match.Success)
            {
                return false;
            }
            return true;
        }
        public static int GetMilliseconds(int frames, int frameRate)
        {
            int milliseconds = (1000 * frames) / frameRate;
            return milliseconds;
        }
        public static int GetFrames(int milliseconds, int frameRate)
        {
            int frames = (milliseconds * frameRate) / 1000;
            return frames;
        }

    }
    public class TimeCodeCalculator
    {
        //Input time must be HH:MM:SS:FF and Output time is HHMMSSFF
        public string Add(string inputTime1, string inputTime2, int frameRate)
        {
            Utility utility = new Utility();

            if (utility.ValidateInputField(inputTime1) && utility.ValidateInputField(inputTime2))
            {
                var input1 = TimeCode.Time(utility.FormatTimeCodeToString(inputTime1));
                var input2 = TimeCode.Time(utility.FormatTimeCodeToString(inputTime2));

                var milliseconds = Utility.GetMilliseconds(input1.Frames, frameRate);
                var adder = new TimeSpan(0, input1.Hours, input1.Minutes, input1.Seconds, milliseconds);
                milliseconds = Utility.GetMilliseconds(input2.Frames, frameRate);
                var addend = new TimeSpan(0, input2.Hours, input2.Minutes, input2.Seconds, milliseconds);

                var sum = adder + addend;

                var timeCode = new TimeCode();
                timeCode.Hours = sum.Hours;
                timeCode.Minutes = sum.Minutes;
                timeCode.Seconds = sum.Seconds;
                timeCode.Frames = Utility.GetFrames(sum.Milliseconds, frameRate);

                if (utility.ValidateOutputField(timeCode.ToString()))
                {
                    return timeCode.ToString();
                }
            }
            return null;
        }
        public string Substract(string intputTime1, string intputTime2, int frameRate)
        {
            Utility utility = new Utility();
            if (utility.ValidateInputField(intputTime1) && utility.ValidateInputField(intputTime2))
            {
                var time2 = TimeCode.Time(utility.FormatTimeCodeToString(intputTime2));
                var time1 = TimeCode.Time(utility.FormatTimeCodeToString(intputTime1));

                var milliseconds = Utility.GetMilliseconds(time1.Frames, frameRate);
                var endTime = new TimeSpan(0, time1.Hours, time1.Minutes, time1.Seconds, milliseconds);

                milliseconds = Utility.GetMilliseconds(time2.Frames, frameRate);
                var startTime = new TimeSpan(0, time2.Hours, time2.Minutes, time2.Seconds, milliseconds);

                var sum = endTime - startTime;

                var timeCode = new TimeCode();
                timeCode.Hours = sum.Hours;
                timeCode.Minutes = sum.Minutes;
                timeCode.Seconds = sum.Seconds;
                timeCode.Frames = Utility.GetFrames(sum.Milliseconds, frameRate);

                if (utility.ValidateOutputField(timeCode.ToString()))
                {
                    return timeCode.ToString();
                }
            }
            return null;
        }

        public int TimeUnitSubstract(int timeUnit, int subtractive, out int reminderValue)
        {
            reminderValue = 0;

            if (timeUnit >= subtractive)
            {
                timeUnit -= subtractive;
                reminderValue = 1;
            }
            return timeUnit;
        }

    }
    public class TimeCode
    {
        private string ToStringFormat = "00";
        public static TimeCode Time(string timeCode)
        {
            TimeCode time = new TimeCode();
            time.Hours = Convert.ToInt32(timeCode.Substring(0, 2));
            time.Minutes = Convert.ToInt32(timeCode.Substring(2, 2));
            time.Seconds = Convert.ToInt32(timeCode.Substring(4, 2));
            time.Frames = Convert.ToInt32(timeCode.Substring(6, 2));
            return time;
        }

        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public int Frames { get; set; }
        public TimeCode()
        {

        }
        public TimeCode(int hours, int minutes, int seconds, int frames)
        {
            Hours = hours;
            Minutes = minutes;
            Seconds = seconds;
            Frames = frames;
        }

        public override string ToString()
        {
            return Hours.ToString(ToStringFormat)
                + Minutes.ToString(ToStringFormat)
                + Seconds.ToString(ToStringFormat)
                + Frames.ToString(ToStringFormat);
        }

    }
}

