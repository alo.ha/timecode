﻿using System;
using TimeCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TimeCodeCalculatorTest
{
    [TestClass]
    public class UtilityTests
    {
        [TestMethod]
        public void ValidationTimeCodeFormatTrue()
        {
            Utility validation = new Utility();
            string timecode = "01:02:22:25";
            bool result = validation.ValidateTimeCodeFormat(timecode);
            Assert.IsTrue(result, String.Format("Expected for '{0}: true; Actual: {1}", timecode, result));
        }
        [TestMethod]
        public void ValidationTimeCodeFormatFalse()
        {
            Utility validation = new Utility();
            string timecode = "01:02:22.25";
            bool result = validation.ValidateTimeCodeFormat(timecode);
            Assert.IsFalse(result, String.Format("Expected for '{0}: false; Actual: {1}", timecode, result));
        }
        [TestMethod]
        public void ValidationTimeCodeFormatHHMMSSReturnFalse()
        {
            Utility validation = new Utility();
            string timecode = "01:02:22";
            bool result = validation.ValidateTimeCodeFormat(timecode);
            Assert.IsFalse(result, String.Format("Expected for '{0}: false; Actual: {1}", timecode, result));
        }
        [TestMethod]
        public void FormatTimeCodeToStringReturnForCorrectStringTrue()
        {
            Utility utility = new Utility();
            string timecode = "01:02:34:56";
            string expected = "01023456";
            string actual = utility.FormatTimeCodeToString(timecode);
            Assert.AreEqual(expected, actual, String.Format("Expected for {0}; Actual: {1}", expected, actual));
        }
        [TestMethod]
        public void FormatTimeCodeToStringReturnNull()
        {
            Utility utility = new Utility();
            string timecode = "01:02:22.25";
            string expected = "01022225";
            string actual = utility.FormatTimeCodeToString(timecode);
            Assert.IsNull(actual, String.Format("Expected for {0}: null; Actual: {1}", expected, actual));
        }
        [TestMethod]
        public void FormatTimeCodeToStringHHMMSSReturnNull()
        {
            Utility utility = new Utility();
            string timecode = "01:02:22";
            string expected = "01022225";
            string actual = utility.FormatTimeCodeToString(timecode);
            Assert.IsNull(actual, String.Format("Expected for {0}: null; Actual: {1}", expected, actual));
        }
        [TestMethod]
        public void FormatTimeCodeToStringNegativeTimeCodeReturnNull()
        {
            Utility utility = new Utility();
            string timecode = "-01:02:22:25";
            string actual = utility.FormatTimeCodeToString(timecode);
            Assert.IsNull(actual, String.Format("Expected for null; Actual: {0}", actual));
        }
        [TestMethod]
        public void ValidateTimeUnitPositiveTimeUnitReturnTrue()
        {
            Utility utility = new Utility();
            int timeUnit = 1;
            bool actual = utility.ValidateTimeUnit(timeUnit);
            Assert.IsTrue(actual, String.Format("Expected for {0}: false; Actual: {1}", timeUnit.ToString(), actual));
        }
        [TestMethod]
        public void ValidateTimeUnitNegativeTimeUnitReturnFalse()
        {
            Utility utility = new Utility();
            int timeUnit = -11;
            bool actual = utility.ValidateTimeUnit(timeUnit);
            Assert.IsFalse(actual, String.Format("Expected for {0}: false; Actual: {1}", timeUnit.ToString(), actual));
        }
        [TestMethod]
        public void ValidateInputFieldPositiveTimeUnitReturnTrue()
        {
            Utility utility = new Utility();
            string timeUnit = "01:02:22:25";
            bool actual = utility.ValidateInputField(timeUnit);
            Assert.IsTrue(actual, String.Format("Expected for {0}: true; Actual: {1}", timeUnit, actual));
        }
        [TestMethod]
        public void ValidateInputFieldNegativeTimeUnitReturnFalse()
        {
            Utility utility = new Utility();
            string timeUnit = "-01:02:22.25";
            bool actual = utility.ValidateInputField(timeUnit);
            Assert.IsFalse(actual, String.Format("Expected for {0}: False; Actual: {1}", timeUnit, actual));
        }
        [TestMethod]
        public void ValidateOutputFieldPositiveTimeCodeReturnTrue()
        {
            Utility utility = new Utility();
            string timeUnit = "01022225";
            bool actual = utility.ValidateOutputField(timeUnit);
            Assert.IsTrue(actual, String.Format("Expected for {0}: true; Actual: {1}", timeUnit, actual));
        }
        [TestMethod]
        public void ValidateOutputFielddNegativeTimeCodeReturnFalse()
        {
            Utility utility = new Utility();
            string timeUnit = "-01022225";
            bool actual = utility.ValidateOutputField(timeUnit);
            Assert.IsFalse(actual, String.Format("Expected for {0}: true; Actual: {1}", timeUnit, actual));
        }
    }
    [TestClass]
    public class TimeCodeCalculatorTests
    {
        [TestMethod]
        public void AddPositiveAdderAndPositiveAddendReturnString()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "01:02:34:05";
            string addend = "02:03:35:06";
            string sum = calculator.Add(adder, addend, 25);
            string expected = "03060911";
            Assert.AreEqual(expected, sum, String.Format("Expected for {0}; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddPositiveAdderAndNegativeAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "01:02:34:56";
            string addend = "-02:03:45:57";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddNegativeAdderAndPositiveAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "-01:02:34:56";
            string addend = "02:03:45:57";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddEmptyAdderAndPositiveAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "";
            string addend = "02:03:45:57";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddPositiveAdderAndEmptyAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "01:02:34:56";
            string addend = "";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddPositiveAdderAndShortenedPositiveAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "01:02:34:56";
            string addend = "02:03:45";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void AddPositiveShortenedAdderAndPositiveAddendReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string adder = "01:02:34";
            string addend = "02:03:45:57";
            string sum = calculator.Add(adder, addend, 25);
            string expected = null;
            Assert.IsNull(sum, String.Format("Expected for {0} to be empty; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void SubstractPositiveInputTime1AndPositiveInputTime2ReturnString()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string inputtime2 = "01:02:36:07";
            string inputtime1 = "01:03:35:06";
            string sum = calculator.Substract(inputtime1, inputtime2, 25);
            string expected = "00005824";
            Assert.AreEqual(expected, sum, String.Format("Expected for {0}; Actual: {1}", expected, sum));
        }
        [TestMethod]
        public void SubstractInputTime2IsLargerThenInputTime1ReturnNull()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            string inputtime1 = "01:02:34:05";
            string inputtime2 = "02:03:35:06";

            string sum = calculator.Substract(inputtime1, inputtime2, 25);

            Assert.IsNull(sum, String.Format("Expected to be null; Actual: {0}", sum));
        }
        [TestMethod]
        public void SubstractTimeUnitPositiveTimeUnitReturnPositiveReminder()
        {
            TimeCodeCalculator calculator = new TimeCodeCalculator();
            int subtractive = 25;
            int timeUnit = 30;
            int expected = 5;
            int reminderExpected = 0;
            int actual = calculator.TimeUnitSubstract(timeUnit, subtractive, out reminderExpected);
            Assert.AreEqual(expected, actual, String.Format("Expected for {0} to be 5; Actual: {1} ", timeUnit, actual));
        }
    }
}

